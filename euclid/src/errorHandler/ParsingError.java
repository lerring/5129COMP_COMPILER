package errorHandler;

import tokens.IdentToken;
import tokens.Token;
import tokens.TokenType;

public class ParsingError extends GenError {

	Token tok;
	String issueType;

	// Class for when error is found during parsing phases.
	public ParsingError(Token tok, String issueType){
		super(tok.getData(), tok.getLineNo(), issueType);
		this.tok = tok;
		this.issueType = issueType;
	}

	public void print() {
		System.out.println("----------------------------------------------\n");
		System.out.println("PARSING ERROR:");
		System.out.println("System recieved unexpected token\n");
		System.out.println("Line Num: " + lineNo);
		System.out.println("Expected: " + issueType);
		
		if (tok.returnType().equals(TokenType.ID)) {
			IdentToken id = (IdentToken) tok;
			System.out.println("Recieved: " + id.getIdName() + "\n");
		} else {
			System.out.println("Recieved: " + tok.getData() + "\n");
		}
		
	}
}
