package errorHandler;


public class LexicalError extends GenError {

	String desc;
	
	// Class for errors found during lexical analysis.
	public LexicalError(String desc, String value, int lineNo, String tokType){
		super(value, lineNo, tokType);
		this.desc = desc;
	}
	
	public void print(){
		
		System.out.println("----------------------------------------------\n");
		System.out.println("LEXICAL ERROR:");
		System.out.println(issueType +" "+ desc + "\n");
		System.out.println("Line Num: " + lineNo);
		System.out.println("Token <" + value +">");
		System.out.println("Expecting: " +issueType+" Token\n");
	}
}
