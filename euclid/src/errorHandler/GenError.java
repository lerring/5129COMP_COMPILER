package errorHandler;

public class GenError {
	
	public String value;
	public int lineNo;
	public String issueType;
	
	// Generic error class, mainly used as a shell.
	public GenError(String value, int lineNo, String issueType){
		
		this.value = value;
		this.lineNo = lineNo;
		this.issueType = issueType;
	}
	
	public void print(){
		System.out.println("----------------------------------------------\n");
		System.out.println("Tokentype: " + issueType);
		System.out.println("Data:      " + value);
		System.out.println("Line Num:      " + lineNo);
	}
}
