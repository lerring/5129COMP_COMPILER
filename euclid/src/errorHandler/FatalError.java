package errorHandler;

import tokens.IdentToken;
import tokens.Token;
import tokens.TokenType;

public class FatalError extends GenError {

	public enum ErrorType {
	    ENDWHILE,
	    ENDIF,
		DUPVAR,
		NOVAR,
		NODEC;
	}
	
	Token tok;
	String tokenType;
	ErrorType errType;
	ParsingError parseErr;

	
	// Errors used to detect flaws in code which cause specified issues.
	public FatalError(ParsingError parseError, ErrorType errType){
		super(parseError.value, parseError.lineNo, parseError.issueType);
		this.errType = errType;
		this.tok = parseError.tok;
		this.parseErr = parseError;
	}

	public void print() {
		System.out.println("----------------------------------------------\n");
		System.out.println("FATAL ERROR:");
		switch(errType){
		
			case ENDIF:{
				System.out.println("System Could Not Find ENDIF in code, programme can not be run.\n");
				System.out.println("Line Num:     " + lineNo);
				System.out.println("Expected: " + issueType);
				
				if (tok.returnType().equals(TokenType.ID)) {
					IdentToken id = (IdentToken) tok;
					System.out.println("Recieved: " + id.getIdName() + "\n");
				} else {
					System.out.println("Recieved: " + tok.getData() + "\n");
				}
				break;
			}
			
			case ENDWHILE:{
				System.out.println("System could not find ENDWHILE in code, programme can not be run.\n");
				System.out.println("Line Num:     " + lineNo);
				System.out.println("Expected: " + issueType);
				
				if (tok.returnType().equals(TokenType.ID)) {
					IdentToken id = (IdentToken) tok;
					System.out.println("Recieved: " + id.getIdName() + "\n");
				} else {
					System.out.println("Recieved: " + tok.getData() + "\n");
				}
				break;
			}
			
			case DUPVAR:{
				System.out.println("System can only initialise variables once.\n");
				System.out.println("Line Num: " + lineNo);
				System.out.println("Expected: New Variable");
				IdentToken id = (IdentToken) tok;
				System.out.println("Recieved: " + id.getIdName() + "\n");
				
				break;
			}
			
			case NOVAR: {
				System.out.println("System could not find the identifier specified.");
				System.out.println("Line Num: " + lineNo);
				IdentToken id = (IdentToken) tok;

				System.out.println("Expected: Token "+id.getIdName()+" Declared Variable" );
				System.out.println("Recieved: " + id.getIdName() + "\n");
				break;
			}
			
			// For my programme i have assumed that variables must be declared before expressions can be done
			// I could have allowed for expressions to be done with basic maths also but this is how i interpret the spec.
			case NODEC: {
				System.out.println("System Requires At least one declaration at the start");
				System.out.println("Line Num: " + lineNo);
				System.out.println("Expected: " + issueType );
				IdentToken id = (IdentToken) tok;
				System.out.println("Recieved: " + id.getIdName() + "\n");
				break;
			}
		}
		
	}
}
