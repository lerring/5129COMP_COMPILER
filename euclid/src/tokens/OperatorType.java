package tokens;

public enum OperatorType {
    PLUS('+'), MINUS('-'), MULTIPLY('*'), MODULUS('%'), GREATER_THAN('>'), LESS_THAN('<'), NOT_EQUAL('!');
    private final char chr;
	
	private OperatorType(char chr){
		this.chr = chr;
	}
	
	public char toChar(){
		return this.chr;
	}
	
	public static OperatorType getFrom(char chr){
		
		for (OperatorType c : OperatorType.values())
			if (c.chr == chr)
				return c;
		
		throw new IllegalArgumentException();
	}
}
