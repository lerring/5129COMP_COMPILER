package tokens;

public class KeywordToken extends Token {
    private String keyword;
    
    public KeywordToken(String keywordVal) {
        super(TokenType.KEYWORD);
        keyword = keywordVal;
    }
    
    public String getKeyword() {
        return keyword;
    }
    
    public void print() {
        System.out.println("Keyword Token: " + keyword);
    }
        
    public String getData(){
    	return keyword;
    }
}
