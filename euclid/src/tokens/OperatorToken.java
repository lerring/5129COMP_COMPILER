
package tokens;

public class OperatorToken  extends Token {
    private OperatorType opType;
    
    public OperatorToken(OperatorType thisOp) {
        super(TokenType.OPERATOR);
        opType = thisOp;
    }
    
    public OperatorType getType() {
        return opType;
    }
    
    public void print() {
        switch(opType) {
            case PLUS: System.out.println("Operator Token: PLUS"); break;
            case MINUS: System.out.println("Operator Token: MINUS"); break;
            case MULTIPLY: System.out.println("Operator Token: MULTIPLY"); break;
            case MODULUS: System.out.println("Operator Token: MODULUS"); break;
            case GREATER_THAN: System.out.println("Operator Token: GREATER_THAN"); break;
            case LESS_THAN: System.out.println("Operator Token: LESS_THAN"); break;
            case NOT_EQUAL: System.out.println("Operator Token: NOT_EQUAL"); break;
        }
    }
    
    public String getData() {
        switch(opType) {
            case PLUS: return "+"; 
            case MINUS: return "-";
            case MULTIPLY: return "*";
            case MODULUS: return "%";
            case GREATER_THAN: return ">";
            case LESS_THAN: return "<";
            case NOT_EQUAL: return "!";
        }
		return "";
    }
    
}
