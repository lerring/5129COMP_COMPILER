
package tokens;

public class NumToken extends Token {
    private int intValue;
    
    public NumToken(int value) {
        super(TokenType.NUM);
        intValue = value;
    }
    
    public int getValue() {
        return intValue;
    }
    
    public void print() {
        System.out.println("Num Token: " + intValue);
    }
       
    public String getData(){
    	return Integer.toString(intValue);
    }
}
