
package tokens;

public class CommentToken extends Token {
   
	public String commentContent;
   
    
    public CommentToken(String commentContent) {
        super(TokenType.COMMENT);
        this.commentContent = commentContent;
    }

    public String getcommonContent() {
        return commentContent;
    }
      
    public void print() {
        System.out.println("Comment Token:\n" + commentContent);
    }
    
    public String getData(){
    	return commentContent;
    }
}
