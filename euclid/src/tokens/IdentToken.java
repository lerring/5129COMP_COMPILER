
package tokens;

public class IdentToken extends Token {
    private String identifierName;
    private int value;
    private boolean enabled;
    
    public IdentToken(String identName) {
        super(TokenType.ID);
        identifierName = identName;
        value = 0;
        enabled = false;
    }

    public String getIdName() {
        return identifierName;
    }
    
    public boolean isEnabled(){
    	return enabled;
    }
    
    public void setValue(int newValue) {
        value = newValue;
    }
    
    public int getValue() {
        return value;
    }
    
    public void print() {
        System.out.println("Identifier Token: " + identifierName);
    }
    
    public String getData(){
    	return Integer.toString(value);
    }

	public void enable() {
		this.enabled = true;
	}
}
