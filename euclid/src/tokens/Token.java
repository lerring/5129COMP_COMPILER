package tokens;

import euclid.LexAnalyser;

// Basic class for tokens, can be extended by subclasses for each particular token
public class Token {
    private TokenType thisToken;
    private int lineNo;
    
    public Token() {
        setThisToken(TokenType.NULL_TOKEN);
    }
    
    public Token(TokenType inputToken) {
        this.setThisToken(inputToken);
        this.lineNo = LexAnalyser.getLineNumber();
    }
    
    public TokenType returnType() {
        return getThisToken();
    }
    
    public void print() {
        switch(getThisToken()) {
            case OPERATOR:	  System.out.println("OPERATOR Token"); 	break;
            case END_OF_FILE: System.out.println("END_OF_FILE Token"); 	break;
            case END_OF_LINE: System.out.println("END_OF_LINE Token"); 	break;
            case NUM:         System.out.println("NUM Token"); 			break;
            case ID:		  System.out.println("ID Token");			break;
            case KEYWORD:	  System.out.println("KEYWORD Token");		break;
            case LBRACKET:	  System.out.println("LBRACKET Token");		break;
            case RBRACKET:	  System.out.println("RBRACKET Token");		break;
            case NULL_TOKEN:  System.out.println("NULL Token"); 		break;
            case COMMENT:	  System.out.println("COMMENT Token"); 		break;
        }
    }
    
    public String toString(){
    	return getThisToken().toString();
    }
    
    public String getData(){
    	switch(getThisToken()){
    	  case END_OF_FILE: 	return "$"; 
          case END_OF_LINE:		return ";";
          case LBRACKET:	  	return "(";
          case RBRACKET:	 	return ")"; 
          default: 				return "";	
    	}
    }
    
    public void setLineNo(int lineNo){
    	this.lineNo = lineNo;
    }
    public int getLineNo(){
    	return this.lineNo;
    }
    
	public TokenType getThisToken() {
		return thisToken;
	}

	public void setThisToken(TokenType thisToken) {
		this.thisToken = thisToken;
	}
}