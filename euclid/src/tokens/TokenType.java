package tokens;

public enum TokenType {
    NUM,
    ID,
    OPERATOR,
    KEYWORD,
    END_OF_FILE,
    END_OF_LINE,
    LBRACKET,
    RBRACKET,
    NULL_TOKEN,
    COMMENT;
}
