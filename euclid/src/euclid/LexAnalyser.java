
package euclid;

import java.io.IOException;
import java.util.Hashtable;

import errorHandler.LexicalError;
import tokens.CommentToken;
import tokens.IdentToken;
import tokens.KeywordToken;
import tokens.NumToken;
import tokens.OperatorToken;
import tokens.OperatorType;
import tokens.Token;
import tokens.TokenType;

public class LexAnalyser {
	
	private static int lineNumber = 1;
	private char peek = ' ';
	private Hashtable<String, IdentToken> identifiers = new Hashtable<String, IdentToken>();
	private Hashtable<String, KeywordToken> reservedKeywords = new Hashtable<String, KeywordToken>();
	
	public LexAnalyser() {

		// When a LexAnalyser object is created, populate the reservedKeywords
		// hashtable with all keywords
		reservedKeywords.put("WHILE", new KeywordToken("WHILE"));
		reservedKeywords.put("IF", new KeywordToken("IF"));
		reservedKeywords.put("THEN", new KeywordToken("THEN"));
		reservedKeywords.put("ENDIF", new KeywordToken("ENDIF"));
		reservedKeywords.put("ENDWHILE", new KeywordToken("ENDWHILE"));
		reservedKeywords.put("INT", new KeywordToken("INT"));
		reservedKeywords.put("OUTPUT", new KeywordToken("OUTPUT"));
		reservedKeywords.put("IS", new KeywordToken("IS"));

	}

	// This next method is for you to write, to scan the input and return the
	// correct Token for the next lexeme
	// The comments inside the scan() method may help you to structure your code
	public Token scan() throws IOException {

		// Loop through all of the peek untill space
		for (;; peek = getChar()) {
			
			if (peek == '\n'){
				lineNumber++;
				setLineNumber(lineNumber);
				continue;
			} else if (peek == ' ' || peek == '\t' || peek == '\r' )
				continue;
			else
				break;
		}

		// Check if the next token an operator or end of line/end of file symbol
		switch (peek) {
			case ('$'): peek = ' ';	return new Token(TokenType.END_OF_FILE);
			case (';'):	peek = ' '; return new Token(TokenType.END_OF_LINE);
			case ('('):	peek = ' ';	return new Token(TokenType.LBRACKET);
			case (')'):	peek = ' '; return new Token(TokenType.RBRACKET);
			case ('<'):	peek = ' ';	return new OperatorToken(OperatorType.LESS_THAN);
			case ('>'):	peek = ' '; return new OperatorToken(OperatorType.GREATER_THAN);
			case ('-'):	peek = ' ';	return new OperatorToken(OperatorType.MINUS);
			case ('%'):	peek = ' ';	return new OperatorToken(OperatorType.MODULUS);
			case ('*'): peek = ' ';	return new OperatorToken(OperatorType.MULTIPLY);
			case ('+'):	peek = ' ';	return new OperatorToken(OperatorType.PLUS);
			case ('!'):	peek = ' ';	return new OperatorToken(OperatorType.NOT_EQUAL);
		}

		//Check for comments to remove
		if (peek == '/') {

			StringBuffer s = new StringBuffer();
			s.append(peek);
			peek = getChar();

			if (peek == '/') {

				do {
					s.append(peek);
					peek = getChar();
				} while (peek != '\n');
				peek = ' ';
				 
				return new CommentToken(s.toString());
			}
						
			else if (peek == '*') {

				do {
					s.append(peek);
					peek = getChar();
				} while (peek != '*');

				s.append(peek);
				peek = getChar();

				if (peek == '/') {
					s.append(peek);
					peek = ' ';
				
					return new CommentToken(s.toString());
				}
			}
		}

		// Is the next token a number? Check whether peek is a digit (from 0-9)
		if (Character.isDigit(peek)) {
			int v = 0;

			do {
				v = 10 * v + Character.digit(peek, 10);
				peek = (char) System.in.read();
			} while (Character.isDigit(peek));
				
			//v is the value of the token as an INT
			return new NumToken(v);
		}

		// Otherwise, check if the next token is a valid identifier or keyword
		if (Character.isLetter(peek)) {

			// creates a string
			StringBuffer b = makeString();

			String currTokenString = b.toString(); 
			
			// Checks if the string is a keyword
			if (reservedKeywords.containsKey(currTokenString.toUpperCase())) {
				if (currTokenString.matches("[A-Z]*")){
									
					KeywordToken kt = new KeywordToken(currTokenString);
					kt.setLineNo(lineNumber);
					return kt;
					
				}else {
					LexicalError lxErr = new LexicalError("Must be Capitalised", currTokenString, lineNumber, TokenType.KEYWORD.toString());
					Euclid.errorArray.add(lxErr);
							
					Token tok = new Token(TokenType.NULL_TOKEN);
					tok.setLineNo(lineNumber);
					return tok;
				}
			}
			
			// Checks if the string is a predetermined Identifier
			if (identifiers.containsKey(currTokenString) && currTokenString.matches("[A-Z]*")) {
				
				IdentToken ident = new IdentToken(currTokenString);
				ident.setLineNo(lineNumber);
				return ident;
			}

			if (currTokenString.matches("[a-z]*")) {
				IdentToken newIdentifier = new IdentToken(currTokenString);
				addIdentifier(newIdentifier);
				return newIdentifier;
			} else {
				LexicalError lxErr = new LexicalError("Must Be Lower Case", currTokenString, lineNumber,  TokenType.ID.toString());
				Euclid.errorArray.add(lxErr);
				Token tok = new Token(TokenType.NULL_TOKEN);
				tok.setLineNo(lineNumber);
				return tok;
			}

		}

		// If we have gotten to here, we have not matched any token so print an
		// error message and return a NULL_TOKEN
		LexicalError lxErr = new LexicalError("Unrecognised Input", String.valueOf(peek), lineNumber, TokenType.NULL_TOKEN.toString());
		Euclid.errorArray.add(lxErr);
		
		peek = ' ';
			
		return new Token(TokenType.NULL_TOKEN);
	}

	/** Reads through the chars and
	 *  builds a string for the next token
	 *   
	 * @return Returns the next token string
	 * @throws IOException
	 */
	public StringBuffer makeString() throws IOException {
		StringBuffer b = new StringBuffer();
		do {
			b.append(peek);
			peek = getChar();
		} while (Character.isLetterOrDigit(peek));
		return b;
	}

	// Used to get the next character.
	private char getChar() throws IOException {
		return (char) System.in.read();
	}

	public Hashtable<String, IdentToken> getIdentifiers() {
		return identifiers;
	}

	void addIdentifier(IdentToken t) {
		identifiers.put(t.getIdName(), t);
	}
	
	public void setLineNumber(int lineNo){
		LexAnalyser.lineNumber = lineNo;
	}
	
	public static int getLineNumber(){
		return lineNumber;
	}

}