package euclid;

import java.io.IOException;
import java.util.Hashtable;

import errorHandler.FatalError;
import errorHandler.FatalError.ErrorType;
import errorHandler.ParsingError;
import tokens.IdentToken;
import tokens.OperatorToken;
import tokens.Token;
import tokens.TokenType;

public class Parser {

	public Token[] tokens; // Array of tokens to be parsed
	public int position; // Current position in array
	Hashtable<String, IdentToken> vars; // Hashtable of all identifiers

	public Parser(Token[] tokenSeq, Hashtable<String, IdentToken> variables) {
		tokens = tokenSeq;
		position = 0; // Current position in sequence
		vars = variables;

	}

	// The following helper method allows us to check if the current token
	// is of type tokType and if so increment the position counter
	private void match(TokenType tokType) {

		if (tokens[position].returnType() != tokType) {
			ParsingError parsErr = new ParsingError(tokens[position], tokType.toString());
			Euclid.errorArray.add(parsErr);
		}

		position++;

	}

	// The following helper method allows us to check if the current token
	// is of type tokType without incrementing the position counter
	private boolean check(TokenType tokType) {

		if (tokens[position].returnType() != tokType) {
			return false;
		}
		return true;
	}

	// Checks if the current token is the keyword
	private boolean checkKeyword(String keyword) {

		if (tokens[position].getData().equals(keyword)) {
			return true;

		} else {
			return false;
		}
	}

	private void matchKeyword(String keyword) {

		if (!tokens[position].getData().equals(keyword)) {

			ParsingError parsErr = new ParsingError(tokens[position], keyword);
			Euclid.errorArray.add(parsErr);
		}
		position++;
	}

	// Start to parse the program
	public void prog() throws IOException {

		// First parse declarations
		decls();
		// Next parse expressions
		exprs();

		// Finally parse the end of file character
		end_of_file();
	}

	private void end_of_file() {

		if (check(TokenType.END_OF_FILE))
			match(TokenType.END_OF_FILE);

	}

	private void decls() throws IOException {

		decl();

		rest();
	}

	private void decl() throws IOException {

		if (checkKeyword("INT")) {
			matchKeyword("INT");

			checkIfDeclared();
			match(TokenType.ID);
			match(TokenType.END_OF_LINE);

		} else {
			ParsingError parsErr = new ParsingError(tokens[position], "INT");
			FatalError fatErr = new FatalError(parsErr, ErrorType.NODEC);
			Euclid.errorArray.add(fatErr);
		}
	}

	/**
	 * Checks if the current ident token is already enabled.
	 * Adds an error to the array, but system will continue presuming it is. 
	 */
	private void checkIfDeclared() {
		
		if (check(TokenType.ID)){
		IdentToken t = (IdentToken) tokens[position];
		String idName = t.getIdName();
		
		if (vars.get(idName).isEnabled()) {
			fatalErr(idName, ErrorType.DUPVAR);
		} else {
			vars.get(idName).enable();
		}
		}
	}

	private void rest() throws IOException {

		// rest -> decl | e

		if (check(TokenType.KEYWORD))
			if (checkKeyword("INT"))
				decls();

	}

	private void exprs() {
		expr();
		part();
	}

	private void expr() {

		switch (tokens[position].getThisToken()) {

		case ID: {

			IdentToken identToken = (IdentToken) tokens[position];

			match(TokenType.ID);

			if (checkKeyword("IS"))
				matchKeyword("IS");

			vars.get(identToken.getIdName()).setValue(oper());

			if (check(TokenType.END_OF_LINE))
				match(TokenType.END_OF_LINE);

			break;
		}

		case KEYWORD: {

			if (checkKeyword("OUTPUT")) {
				matchKeyword("OUTPUT");

				if (check(TokenType.LBRACKET))
					match(TokenType.LBRACKET);

				if (check(TokenType.ID) || check(TokenType.NUM)) {

					int printNum = var();
					System.out.println(printNum);
				}

				if (check(TokenType.RBRACKET))
					match(TokenType.RBRACKET);

				if (check(TokenType.END_OF_LINE))
					match(TokenType.END_OF_LINE);

				break;
			}

			if (checkKeyword("IF")) {
				matchKeyword("IF");

				if (check(TokenType.LBRACKET))
					match(TokenType.LBRACKET);

				boolean cond = cond();
				if (check(TokenType.RBRACKET))
					match(TokenType.RBRACKET);

				if (checkKeyword("THEN")) {
					matchKeyword("THEN");

				} else {
					ParsingError parsErr = new ParsingError(tokens[position], "THEN");
					Euclid.errorArray.add(parsErr);
				}

				if (cond) {

					exprs();

				} else {
					skipToEndIf();
				}
				break;
			}

			if (checkKeyword("WHILE")) {
				matchKeyword("WHILE");

				if (check(TokenType.LBRACKET))
					match(TokenType.LBRACKET);

				int startPos = position; // saves data for start of loop

				boolean cond;

				do {

					position = startPos;
					cond = cond();

					if (check(TokenType.RBRACKET))
						match(TokenType.RBRACKET);

					if (checkKeyword("THEN"))
						matchKeyword("THEN");
					// match(TokenType.KEYWORD);

					if (cond)
						exprs();
					else
						skipToEndWhile();

				} while (cond);
			}
			break;
		}

		default: {
			parseErr("Expression");
			break;

		}

		}
	}

	/**
	 * 
	 */
	private void parseErr(String str) {
		ParsingError parsErr = new ParsingError(tokens[position], str);
		Euclid.errorArray.add(parsErr);
		Euclid.printErrors();
		System.exit(1);
	}

	private void fatalErr(String str, ErrorType errType) {
		ParsingError parsErr = new ParsingError(tokens[position], str);
		FatalError fatalErr = new FatalError(parsErr, errType);
		Euclid.errorArray.add(fatalErr);
		Euclid.printErrors();
		System.exit(1);
	}

	/**
	 * 
	 */
	private void skipToEndWhile() {

		boolean error = true;

		while (tokens[position] != null) {

			if (checkKeyword("ENDWHILE")) {
				matchKeyword("ENDWHILE");
				// match(TokenType.KEYWORD);

				error = false;
				break;
			}
			position++;
		}

		if (error) {
			position--;
			fatalErr("ENDWHILE", ErrorType.ENDWHILE);

		}
	}

	private void skipToEndIf() {

		boolean error = true;

		while (tokens[position] != null) {
			check(TokenType.END_OF_LINE);
			checkKeyword("THEN");

			if (checkKeyword("ENDIF")) {
				error = false;
				break;
			}
			position++;
		}

		if (error) {
			position--;
			fatalErr("ENDIF", ErrorType.ENDIF);
		}
	}

	private void part() {

		if (check(TokenType.KEYWORD)) {
			if (!checkKeyword("ENDWHILE") && !checkKeyword("ENDIF"))
				exprs();
		} else if (check(TokenType.ID))
			exprs();
	}

	private boolean cond() {

		int num1 = var();
		boolean bool = false;

		if (check(TokenType.OPERATOR)) {
			OperatorToken ot = (OperatorToken) tokens[position];
			int num2;

			switch (ot.getType()) {
			case GREATER_THAN:
				match(TokenType.OPERATOR);
				num2 = var();
				bool = num1 > num2;
				break;
			case LESS_THAN:
				match(TokenType.OPERATOR);
				num2 = var();
				bool = num1 < num2;
				break;
			case NOT_EQUAL:
				match(TokenType.OPERATOR);
				num2 = var();
				bool = num1 != num2;
				break;
			default:
				ParsingError parsErr = new ParsingError(tokens[position], TokenType.OPERATOR.toString());
				Euclid.errorArray.add(parsErr);
				break;
			}
		}
		return bool;
	}

	private int oper() {

		int value = 0;
		int num1 = var();

		if (check(TokenType.OPERATOR)) {

			OperatorToken ot = (OperatorToken) tokens[position];
			int num2;

			switch (ot.getType()) {

			case MINUS:
				match(TokenType.OPERATOR);
				num2 = var();
				value = num1 - num2;
				break;
			case MODULUS:
				match(TokenType.OPERATOR);
				num2 = var();
				value = num1 % num2;
				break;
			case MULTIPLY:
				match(TokenType.OPERATOR);
				num2 = var();
				value = num1 * num2;
				break;
			case PLUS:
				match(TokenType.OPERATOR);
				num2 = var();
				value = num1 + num2;
				break;
			default:
				ParsingError parsErr = new ParsingError(tokens[position], TokenType.OPERATOR.toString());
				Euclid.errorArray.add(parsErr);
				break;
			}
		} else {
			value = num1;
		}

		if (check(TokenType.END_OF_LINE)) {
			match(TokenType.END_OF_LINE);

		} else {
			ParsingError parsErr = new ParsingError(tokens[position], TokenType.END_OF_LINE.toString());
			Euclid.errorArray.add(parsErr);
		}
		return value;

	}

	private int var() {

		switch (tokens[position].getThisToken()) {

		case ID: {
			
			IdentToken ident = (IdentToken) tokens[position];
			
			if (!vars.get(ident.getIdName()).isEnabled()) 
				fatalErr(ident.getIdName(), ErrorType.NOVAR);
				
			match(TokenType.ID);
			return vars.get(ident.getIdName()).getValue();
		}
		
		case NUM: {
			int num = Integer.parseInt(tokens[position].getData());
			match(TokenType.NUM);
			return num;
		}
		
		default:
			ParsingError parsErr = new ParsingError(tokens[position], "VARIABLE");
			Euclid.errorArray.add(parsErr);
		}

		return 0;
	}
}