/*
 * Euclid Language Skeleton Java program
 * You may use these .java files as a start for your coursework
 * All other coding should be your own (individual coursework)
 */
package euclid;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import errorHandler.GenError;
import tokens.IdentToken;
import tokens.Token;
import tokens.TokenType;

public class Euclid {

	public static final int MAX_NUM_TOKENS = 3000; 
	public static int currentToken = 0;
	public static boolean lexAnalysisSuccessful = true;

	public static Token[] tokenSequence = new Token[MAX_NUM_TOKENS];
	public static Hashtable<String, IdentToken> idents;
	public static ArrayList<GenError> errorArray = new ArrayList<GenError>();

	public static void main(String[] args) throws IOException {
		LexAnalyser lex = lexiScan();
		parser(lex);
	}

	/**
	 * Method to provide the parsing functionality
	 * within the code
	 *
	 * @param lex - the current lex analyzer
	 * @throws IOException
	 */
	public static void parser(LexAnalyser lex) throws IOException {

		// Lexical analysis complete, now on to parsing..
		System.out.println("\n--- Beginning Parsing ---\n");
		
		idents = lex.getIdentifiers();
		
		// This next declaration passes the sequence of tokens and hashtable of
		// identifiers to initialize the parser.
		Parser pars = new Parser(tokenSequence, idents);
		pars.prog();
		
		System.out.println("\n--- Ending Parsing ---\n");
		
		if (!errorArray.isEmpty()) {
			System.out.println("Error in Parsing phase of the program.\n");
			printErrors();
			System.exit(1);
		} else {
			System.out.println("Parsing Successful \n");
		}
	}

	/**
	 * 
	 * @return
	 */
	public static LexAnalyser lexiScan() {

		LexAnalyser lx = new LexAnalyser();
		Token nextToken = new Token(TokenType.END_OF_LINE);

		do {
			try {

				nextToken = lx.scan();

				if (nextToken.returnType() == TokenType.NULL_TOKEN) {
					lexAnalysisSuccessful = false;
					continue;
				}

				if (nextToken.returnType() == TokenType.COMMENT) {
					continue;
				}

				tokenSequence[currentToken] = nextToken;
				currentToken++;

			} catch (IOException ex) {
				System.out.println("Error Reading in token: \n" + tokenSequence[currentToken].getData());
			}

		} while (nextToken.returnType() != TokenType.END_OF_FILE);

		if (lexAnalysisSuccessful) {
			System.out.println("Lexical analysis successful.\n");
		} else {
			System.out.println("Error in lexical analysis of the program.\n");
			printErrors();
			System.exit(1);
		}

		printTokens();
		return lx;
	}

	public static void printTokens() {
		for (int i = 0; i < currentToken; i++) {
			tokenSequence[i].print();
		}
	}

	public static void printErrors() {

		for (GenError err : errorArray) {
			err.print();
		}
	}
}
